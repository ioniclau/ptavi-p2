import sys
import calcoohija


if __name__ == "__main__":
    fichero = open(sys.argv[1])
    lista = fichero.readlines()
    for line in lista:
        elementos = line.split(",")
        operando = elementos[0]
        valortemporal = int(elementos[1])
        for numero in elementos[2:]:

            operador = int(numero)
            objeto = calcoohija.CalculadoraHija(valortemporal, operador)

            if operando == "suma":
                valortemporal = objeto.plus()

            elif operando == "resta":
                valortemporal = objeto.minus()

            elif operando == "multiplica":
                valortemporal = objeto.multi()

            elif operando == "divide":
                valortemporal = objeto.div()

            else:
                sys.exit("Solo puedes sumar, restar, multiplicar o dividir.")
        print(valortemporal)
