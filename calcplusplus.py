import sys
import calcoohija
import csv

with open(sys.argv[1], newline="") as fichero:
    lista = csv.reader(fichero)

    for line in lista:

        operando = line[0]
        valortemporal = int(line[1])

        for numero in line[2:]:

            operador = int(numero)
            objeto = calcoohija.CalculadoraHija(valortemporal, operador)

            if operando == "suma":
                valortemporal = objeto.plus()

            elif operando == "resta":
                valortemporal = objeto.minus()

            elif operando == "multiplica":
                valortemporal = objeto.multi()

            elif operando == "divide":
                valortemporal = objeto.div()

            else:
                sys.exit("Solo puedes sumar, restar, multiplicar o dividir.")
        print(valortemporal)
